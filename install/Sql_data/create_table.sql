-- phpMyAdmin SQL Dump
-- version phpStudy 2014
-- http://www.phpmyadmin.net
--
-- 主机: localhost
-- 生成日期: 2019 �?01 �?14 �?06:21
-- 服务器版本: 5.5.53
-- PHP 版本: 5.6.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- 数据库: `wmcms`
--

-- --------------------------------------------------------

--
-- 表的结构 `wk_ad`
--

CREATE TABLE IF NOT EXISTS `wk_ad` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `board_id` smallint(5) NOT NULL,
  `type` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `extimg` varchar(255) NOT NULL,
  `extval` varchar(200) NOT NULL,
  `desc` varchar(255) NOT NULL,
  `start_time` int(10) NOT NULL,
  `end_time` int(10) NOT NULL,
  `clicks` int(10) NOT NULL DEFAULT '0',
  `add_time` int(10) NOT NULL DEFAULT '0',
  `ordid` tinyint(3) unsigned NOT NULL DEFAULT '255',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_adboard`
--

CREATE TABLE IF NOT EXISTS `wk_adboard` (
  `id` smallint(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `tpl` varchar(20) NOT NULL,
  `width` smallint(5) NOT NULL,
  `height` smallint(5) NOT NULL,
  `description` varchar(255) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_admin_auth`
--

CREATE TABLE IF NOT EXISTS `wk_admin_auth` (
  `role_id` tinyint(3) NOT NULL,
  `menu_id` smallint(6) NOT NULL,
  KEY `role_id` (`role_id`,`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `wk_article`
--

CREATE TABLE IF NOT EXISTS `wk_article` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `cateid` int(10) NOT NULL,
  `intro` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `color` varchar(10) NOT NULL,
  `uid` int(20) NOT NULL,
  `tags` varchar(100) NOT NULL,
  `add_time` int(10) NOT NULL,
  `top` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '置顶 1是0否',
  `hits` int(10) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '，0未审核，1已审核，2精华，3删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_article_cate`
--

CREATE TABLE IF NOT EXISTS `wk_article_cate` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `img` varchar(255) NOT NULL,
  `pid` smallint(4) unsigned NOT NULL DEFAULT '0',
  `spid` varchar(50) NOT NULL,
  `ordid` smallint(4) unsigned NOT NULL DEFAULT '255',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=20 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_badword`
--

CREATE TABLE IF NOT EXISTS `wk_badword` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `word_type` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1：禁用；2：替换；3：审核',
  `badword` varchar(100) NOT NULL,
  `replaceword` varchar(100) NOT NULL,
  `add_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_bindmail`
--

CREATE TABLE IF NOT EXISTS `wk_bindmail` (
  `uid` int(20) NOT NULL,
  `bind` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `wk_comment`
--

CREATE TABLE IF NOT EXISTS `wk_comment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `typeid` smallint(4) unsigned NOT NULL,
  `itemid` int(20) unsigned NOT NULL,
  `commentid` int(10) NOT NULL DEFAULT '1',
  `toid` int(10) NOT NULL,
  `info` varchar(255) NOT NULL,
  `uid` int(20) NOT NULL,
  `add_time` int(10) NOT NULL,
  `youyong` int(10) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `caina` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_doc_cate`
--

CREATE TABLE IF NOT EXISTS `wk_doc_cate` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `img` varchar(255) NOT NULL,
  `pid` smallint(4) unsigned NOT NULL DEFAULT '0',
  `spid` varchar(50) NOT NULL,
  `ordid` smallint(4) unsigned NOT NULL DEFAULT '255',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=58 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_doc_con`
--

CREATE TABLE IF NOT EXISTS `wk_doc_con` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `cateid` smallint(4) unsigned NOT NULL,
  `zhuanji` smallint(4) unsigned NOT NULL COMMENT '所属专辑ID',
  `title` varchar(50) NOT NULL,
  `intro` varchar(255) NOT NULL,
  `score` int(10) NOT NULL DEFAULT '0',
  `ext` varchar(10) NOT NULL,
  `filesize` int(50) NOT NULL,
  `filename` varchar(255) NOT NULL,
  `oldname` varchar(255) NOT NULL,
  `fileurl` varchar(255) NOT NULL,
  `viewurl` varchar(255) NOT NULL,
  `imgurl` varchar(255) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `uid` int(20) NOT NULL,
  `tags` varchar(100) NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '浏览数',
  `add_time` int(10) NOT NULL,
  `status` int(1) DEFAULT '2' COMMENT '1未审核;2已审核;3置顶;4精品',
  `model` tinyint(1) unsigned NOT NULL DEFAULT '3' COMMENT '1:虚拟2:官方3:独立',
  `html` text NOT NULL,
  `page` int(11) DEFAULT '0',
  `convert_key` varchar(100) DEFAULT '',
  `convert_status` int(1) DEFAULT '1' COMMENT '1转换中;2已转换;3失败',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=438 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_flink`
--

CREATE TABLE IF NOT EXISTS `wk_flink` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `img` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `cate_id` smallint(5) NOT NULL,
  `ordid` tinyint(3) unsigned NOT NULL DEFAULT '255',
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_flink_cate`
--

CREATE TABLE IF NOT EXISTS `wk_flink_cate` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `ordid` tinyint(3) unsigned NOT NULL DEFAULT '255',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_focus`
--

CREATE TABLE IF NOT EXISTS `wk_focus` (
  `uid` int(20) NOT NULL,
  `focusuid` int(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `wk_forum`
--

CREATE TABLE IF NOT EXISTS `wk_forum` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `cateid` int(10) NOT NULL,
  `intro` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `score` int(5) NOT NULL DEFAULT '0' COMMENT '悬赏付出的积分',
  `uid` int(20) NOT NULL,
  `tags` varchar(100) NOT NULL,
  `add_time` int(10) NOT NULL,
  `top` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '置顶 1是0否',
  `caina` int(10) NOT NULL DEFAULT '0',
  `hits` int(10) NOT NULL DEFAULT '0' COMMENT '浏览量',
  `jing` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1加精',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '，0未审核，1已审核，2精华，3删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=45 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_forum_cate`
--

CREATE TABLE IF NOT EXISTS `wk_forum_cate` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `ordid` tinyint(3) unsigned NOT NULL DEFAULT '10',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=90 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_global`
--

CREATE TABLE IF NOT EXISTS `wk_global` (
  `name` varchar(100) NOT NULL,
  `data` text NOT NULL,
  `remark` varchar(255) NOT NULL,
  KEY `name` (`name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `wk_ipban`
--

CREATE TABLE IF NOT EXISTS `wk_ipban` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `type` varchar(30) NOT NULL,
  `expires_time` int(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_itemlog`
--

CREATE TABLE IF NOT EXISTS `wk_itemlog` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(20) NOT NULL,
  `itemid` int(5) NOT NULL,
  `typeid` int(5) NOT NULL COMMENT '1:文档',
  `type` int(5) NOT NULL COMMENT '1:下载2:喜欢3:推荐',
  `add_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_jubao`
--

CREATE TABLE IF NOT EXISTS `wk_jubao` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `content` varchar(500) NOT NULL,
  `typeid` smallint(4) unsigned NOT NULL,
  `itemid` int(20) unsigned NOT NULL,
  `uid` int(20) NOT NULL,
  `add_time` int(10) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_mail_queue`
--

CREATE TABLE IF NOT EXISTS `wk_mail_queue` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mail_to` varchar(120) NOT NULL,
  `mail_subject` varchar(255) NOT NULL,
  `mail_body` text NOT NULL,
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `err_num` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `add_time` int(10) unsigned NOT NULL,
  `lock_expiry` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_menu`
--

CREATE TABLE IF NOT EXISTS `wk_menu` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `pid` smallint(6) NOT NULL,
  `module_name` varchar(20) NOT NULL,
  `action_name` varchar(20) NOT NULL,
  `data` varchar(120) NOT NULL,
  `remark` varchar(255) NOT NULL,
  `often` tinyint(1) NOT NULL DEFAULT '0',
  `ordid` tinyint(3) unsigned NOT NULL DEFAULT '255',
  `style` varchar(10) NOT NULL DEFAULT 'left',
  `ico` varchar(10) NOT NULL DEFAULT 'xe602',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_nav`
--

CREATE TABLE IF NOT EXISTS `wk_nav` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `alias` varchar(20) NOT NULL,
  `link` varchar(255) NOT NULL,
  `target` tinyint(1) NOT NULL DEFAULT '1',
  `ordid` tinyint(3) unsigned NOT NULL DEFAULT '255',
  `mod` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_oauth`
--

CREATE TABLE IF NOT EXISTS `wk_oauth` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(20) NOT NULL,
  `name` varchar(50) NOT NULL,
  `config` text NOT NULL,
  `desc` text NOT NULL,
  `author` varchar(50) NOT NULL,
  `ordid` tinyint(3) unsigned NOT NULL DEFAULT '255',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_page`
--

CREATE TABLE IF NOT EXISTS `wk_page` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `content` text NOT NULL,
  `ordid` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_payment`
--

CREATE TABLE IF NOT EXISTS `wk_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `mark` varchar(255) DEFAULT NULL,
  `description` text,
  `logo` varchar(255) DEFAULT NULL,
  `merchant` varchar(255) DEFAULT NULL,
  `account` varchar(255) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `ordid` int(11) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL COMMENT '0禁用1启用',
  `appid` varchar(100) DEFAULT '',
  `appsecret` varchar(150) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_raty`
--

CREATE TABLE IF NOT EXISTS `wk_raty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `voter` int(10) NOT NULL DEFAULT '0' COMMENT '评分次数',
  `total` int(11) NOT NULL DEFAULT '0' COMMENT '总分',
  `typeid` int(11) NOT NULL DEFAULT '0' COMMENT '1:文档2：活动3：待定',
  `itemid` int(11) NOT NULL COMMENT '内容id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_raty_user`
--

CREATE TABLE IF NOT EXISTS `wk_raty_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ratyid` int(10) NOT NULL,
  `uid` int(10) NOT NULL,
  `score` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_recharge`
--

CREATE TABLE IF NOT EXISTS `wk_recharge` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sn` varchar(255) NOT NULL,
  `uid` int(11) NOT NULL,
  `uname` varchar(20) NOT NULL,
  `score` varchar(255) NOT NULL,
  `cash` varchar(255) NOT NULL,
  `bank_id` varchar(255) NOT NULL,
  `have_pay` tinyint(1) NOT NULL,
  `add_time` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_score_item`
--

CREATE TABLE IF NOT EXISTS `wk_score_item` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `cate_id` smallint(4) unsigned NOT NULL,
  `title` varchar(120) NOT NULL,
  `type` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0：实物；1：虚拟',
  `img` varchar(255) NOT NULL,
  `score` int(10) unsigned NOT NULL DEFAULT '0',
  `stock` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `user_num` mediumint(8) unsigned NOT NULL DEFAULT '1',
  `buy_num` mediumint(8) NOT NULL DEFAULT '0',
  `desc` text NOT NULL,
  `ordid` tinyint(3) unsigned NOT NULL DEFAULT '255',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_score_item_cate`
--

CREATE TABLE IF NOT EXISTS `wk_score_item_cate` (
  `id` smallint(4) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ordid` tinyint(3) unsigned NOT NULL DEFAULT '255',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_score_log`
--

CREATE TABLE IF NOT EXISTS `wk_score_log` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(10) NOT NULL,
  `uname` varchar(50) NOT NULL,
  `action` varchar(50) NOT NULL,
  `score` int(10) NOT NULL,
  `add_time` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=160 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_score_order`
--

CREATE TABLE IF NOT EXISTS `wk_score_order` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_sn` varchar(100) NOT NULL,
  `uid` int(10) unsigned NOT NULL,
  `uname` varchar(20) NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `item_name` varchar(120) NOT NULL,
  `item_num` mediumint(8) NOT NULL,
  `consignee` varchar(20) NOT NULL,
  `address` varchar(255) NOT NULL,
  `zip` varchar(10) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `order_score` int(10) NOT NULL,
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `add_time` int(10) unsigned NOT NULL,
  `remark` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_slide`
--

CREATE TABLE IF NOT EXISTS `wk_slide` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `url` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `add_time` int(10) NOT NULL DEFAULT '0',
  `ordid` int(11) unsigned NOT NULL DEFAULT '255',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_tag`
--

CREATE TABLE IF NOT EXISTS `wk_tag` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=53 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_topiclog`
--

CREATE TABLE IF NOT EXISTS `wk_topiclog` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `uid` int(20) NOT NULL,
  `itemid` int(5) NOT NULL,
  `type` int(5) NOT NULL COMMENT '1:喜欢',
  `istopic` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_txl`
--

CREATE TABLE IF NOT EXISTS `wk_txl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `cate_id` int(10) NOT NULL,
  `tel` varchar(20) NOT NULL,
  `lianxiren` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL,
  `url` varchar(100) NOT NULL,
  `uid` int(11) NOT NULL,
  `img` varchar(255) NOT NULL,
  `renzheng` tinyint(1) NOT NULL DEFAULT '0' COMMENT '认证',
  `add_time` int(11) NOT NULL,
  `hits` int(10) NOT NULL DEFAULT '0' COMMENT '浏览数',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_txl_cate`
--

CREATE TABLE IF NOT EXISTS `wk_txl_cate` (
  `id` smallint(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `ordid` smallint(4) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_txl_jiucuo`
--

CREATE TABLE IF NOT EXISTS `wk_txl_jiucuo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `txl_id` int(11) NOT NULL,
  `content` varchar(200) NOT NULL,
  `add_time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_user`
--

CREATE TABLE IF NOT EXISTS `wk_user` (
  `uid` int(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `avatar` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `uc_id` tinyint(1) NOT NULL DEFAULT '0',
  `roleid` int(10) NOT NULL DEFAULT '2',
  `add_time` int(20) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=653327 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_userinfo`
--

CREATE TABLE IF NOT EXISTS `wk_userinfo` (
  `uid` int(20) NOT NULL,
  `gender` tinyint(1) NOT NULL DEFAULT '2' COMMENT '1男，0女，2保密',
  `tags` varchar(50) NOT NULL COMMENT '个人标签',
  `intro` varchar(500) NOT NULL,
  `byear` smallint(4) unsigned NOT NULL,
  `bmonth` tinyint(2) unsigned NOT NULL,
  `bday` tinyint(2) unsigned NOT NULL,
  `province` varchar(20) NOT NULL,
  `city` varchar(20) NOT NULL,
  `county` varchar(20) NOT NULL,
  `contact` int(20) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `wk_user_bind`
--

CREATE TABLE IF NOT EXISTS `wk_user_bind` (
  `uid` int(10) unsigned NOT NULL,
  `type` varchar(60) NOT NULL,
  `keyid` varchar(100) NOT NULL,
  `info` text NOT NULL,
  KEY `uid` (`uid`),
  KEY `uid_type` (`uid`,`type`),
  KEY `type_keyid` (`type`,`keyid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `wk_user_mail`
--

CREATE TABLE IF NOT EXISTS `wk_user_mail` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `fromid` int(10) NOT NULL,
  `toid` int(10) NOT NULL,
  `title` varchar(30) NOT NULL,
  `info` varchar(255) NOT NULL,
  `re_id` int(10) NOT NULL,
  `is_sys` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0:私信，1:通知',
  `add_time` int(10) NOT NULL,
  `new` tinyint(1) NOT NULL DEFAULT '1',
  `pb` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_user_role`
--

CREATE TABLE IF NOT EXISTS `wk_user_role` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `score` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `isAdmin` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_user_scoresum`
--

CREATE TABLE IF NOT EXISTS `wk_user_scoresum` (
  `uid` int(20) NOT NULL,
  `score` int(10) NOT NULL DEFAULT '0' COMMENT '总积分',
  `doc` int(10) NOT NULL DEFAULT '0' COMMENT '文档所得',
  `down` int(10) NOT NULL DEFAULT '0' COMMENT '下载花费',
  `up` int(10) NOT NULL DEFAULT '0' COMMENT '上传所得',
  `pay` int(10) NOT NULL DEFAULT '0' COMMENT '充值所得',
  `chengfa` int(10) NOT NULL DEFAULT '0' COMMENT '惩罚',
  `register` int(10) NOT NULL DEFAULT '0' COMMENT '注册所得',
  `login` int(10) NOT NULL DEFAULT '0' COMMENT '登录所得',
  `credit` int(10) NOT NULL DEFAULT '0' COMMENT '论坛兑换',
  PRIMARY KEY (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `wk_user_stat`
--

CREATE TABLE IF NOT EXISTS `wk_user_stat` (
  `uid` int(10) unsigned NOT NULL,
  `action` varchar(20) NOT NULL,
  `num` int(10) unsigned NOT NULL,
  `last_time` int(10) unsigned NOT NULL,
  UNIQUE KEY `uid_type` (`uid`,`action`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `wk_withdraw`
--

CREATE TABLE IF NOT EXISTS `wk_withdraw` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL,
  `uname` varchar(20) NOT NULL,
  `cash` varchar(255) NOT NULL,
  `bank_id` varchar(255) NOT NULL,
  `user_account` varchar(255) NOT NULL,
  `realname` varchar(255) NOT NULL,
  `remark` text NOT NULL,
  `add_time` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- 表的结构 `wk_zj`
--

CREATE TABLE IF NOT EXISTS `wk_zj` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `cateid` int(10) NOT NULL,
  `intro` varchar(255) NOT NULL,
  `img` varchar(255) NOT NULL,
  `uid` int(20) NOT NULL,
  `add_time` int(10) NOT NULL,
  `tuijian` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `zhiding` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
